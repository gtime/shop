import React, { FunctionComponent } from 'react';

interface Props{}

const AppBar: FunctionComponent<Props> = () => {
  return (
    <header className="appbar">
      <h1 role="heading">Products</h1> 

      <style jsx>{`
        .appbar {
          height: 12rem;
          display: grid;
          align-content: center;
          padding: 0rem 6%;
        }

        h1 {
          color: var(--color-primary);
          font: normal normal normal 4.2rem/2.4rem 'Poppins Bold'
        }

      `}</style>
    </header>
  );
}

export default AppBar;
