import React, {FunctionComponent} from 'react';
import MenuBar from './MenuBar'


const Layout: FunctionComponent = ({ children }) => {
  return (
    <div className="app">
        <div className="layout">
            <MenuBar/>
            {children}
        </div>

      <style jsx>{`
        .app {
            width: 100vw;
            height: 100vh;
            position: fixed;
            top: 0;
            left: 0;
            display: flex;
            z-index: 1;
        }

        .app::before {
            position: absolute;
            content: '';
            width: 100%;
            height: 100%;
            /* The image used */
            background-image: url("/images/background.jpg");

            /* Add the blur effect */
            filter: blur(8px);
            -webkit-filter: blur(8px);
        
            /* Full height */
            height: 100%;
        
            /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
        .layout {
            width: 100vw;
            height: 100vh;
            margin: auto;
            z-index: +1;
            overflow: hidden;
            display: flex;
            background-color: rgba(255, 255, 255, 0.4);
        }

        .layout nav {
            flex-shrink: 0;
            flex-grow: 0;
            width: 30rem;
            height: 100%;
            padding: 4rem;
            background-color: rgba(255, 255, 255, 0.1);
        }
        `}</style>
    </div>
  );
}

export default Layout;
