import React, {FunctionComponent} from 'react';

const Layout: FunctionComponent = ({ children }) => {
  return (
    <nav>
      <a href="/" className="brand menu-item">
        <img src="/logo512.png" alt="Layout Logo" />
        <span>Simple Shop</span>
      </a>

      <div className="links">
        <a href="#" className="menu-item">
            {/* <img src="#" alt="" /> */}
            <span>Products</span>
        </a>
      </div>

      <style jsx>{`
        nav {
            flex-shrink: 0;
            flex-grow: 0;
            width: 25rem;
            height: 100%;
            background-color: rgba(255, 255, 255, 0.1); 
        }

        .brand, .links {
          padding: 4rem;
        }
        
        .menu-item {
          display: flex;
          align-items: center;
          font: normal normal normal 1.6rem 'Poppins'
        }

        .brand {
          height: 4rem;
          color: var(--color-primary);
          font: normal normal 700 1.8rem 'Poppins ExtraBold'

        }

        img {
            width: 3rem;
            height: 3rem;
            margin-right: .5rem;
            background: var(--color-secondary);
            border-radius: 50%
        }
     `}</style>
    </nav>
  );
}

export default Layout;
