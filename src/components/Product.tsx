import React, { FunctionComponent } from 'react';
import { DenormalizedProduct } from 'src/services/types';

interface Props {
  product: DenormalizedProduct
}

const formateDate = (date: string) => new Date(date).toDateString()

const Product: FunctionComponent<Props> = ({ product }) => {
  const recentPrice = product.prices[0];
  
  return (
    <article className="product">
      <h2>{product.name}</h2>
      {recentPrice && <p>GHC {recentPrice.price}</p>}
      {recentPrice && <p>{formateDate(recentPrice.date)}</p>}

      <style jsx>{`
        .product {
          height: 24rem;
          background: var(--color-secondary);
          display: grid;
          place-content: center;
          transition: box-shadow 0.3s ease-out, transform 0.3s ease-out;
          cursor: pointer;
        }

        .product:hover {
          box-shadow: 0rem 4rem 8rem rgba(0,0,0,0.16);
          transform: scale(1.01);
        }

        h2 {
          color: var(--color-primary);
          font: normal normal normal 1.8rem/2.4rem 'Poppins'
        }
      `}</style>
    </article>
  );
}

export default Product;
