import React, {FunctionComponent} from 'react';
import { useSelector } from 'react-redux'
import { AppState } from '../services/types';
import Product from './Product';

const productSelector = (state: AppState) => {
  return Object.values(state.products).map(product => ({
      ...product,
      prices: product.prices.map(priceId => state.prices[priceId])
  }))
}

const ProductDetails: FunctionComponent = () => {
  const products = useSelector(productSelector);

  return (
    <div className="product-details">
        <div className="price-list">
          {/* {products.map((product, key) => <Product key={key} product={product}/>)} */}
        </div>
      
      <style jsx>{`
        .product-details {
          width: 40rem;
          flex-grow: 0;
          flex-shrink:0;
          padding: 8rem 0rem;
          position: relative;
        }

        .container {
          display: grid;
          grid-template-columns: repeat(auto-fill, minmax(24rem, 1fr));
          gap: 2rem
        }
      `}</style>
    </div>
  );
}

export default ProductDetails;
