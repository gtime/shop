import React, {FunctionComponent} from 'react';
import { useSelector } from 'react-redux'
import { AppState } from '../services/types';
import Product from './Product';
import AppBar from '../components/AppBar';


const productSelector = (state: AppState) => {
  return Object.values(state.products).map(product => ({
      ...product,
      prices: product.prices.map(priceId => state.prices[priceId])
  }))
}

const ProductList: FunctionComponent = () => {
  const products = useSelector(productSelector);

  return (
    <div className="product-list">
        <AppBar/>
        
      <div className="products">
          {products.map((product, key) => <Product key={key} product={product}/>)}
      </div>
      
      <style jsx>{`
        .product-list {
          flex-grow: 1;
          flex-shrink:1;
          position: relative;
          background-color: rgba(255, 255, 255, 0.4);
        }

        .products {
          padding: 8rem 6%;
          display: grid;
          grid-template-columns: repeat(auto-fill, minmax(24rem, 1fr));
          gap: 2rem;
        }
      `}</style>
    </div>
  );
}

export default ProductList;
