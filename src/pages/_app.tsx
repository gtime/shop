import { Provider } from 'react-redux';
import type { AppProps } from 'next/app';

import Layout from '../components/Layout';
import {appStore} from '../services/app';
import '../styles/global.css';

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={appStore}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </Provider>
  )
}

export default MyApp