import { useEffect } from 'react';
import { useDispatch } from 'react-redux'
import { fetchProducts } from '../services/api';
import { NormalizedProduct } from '../services/types';
import ProductList from '../components/ProductList';
import ProductDetails from '../components/ProductDetails';


interface Props {
    products: NormalizedProduct
}

export default function Home({ products }: Props) {    
    const dispatch = useDispatch();

    // Load fetch state
    useEffect(() => {
        dispatch({ type: 'product:fetched', payload: products })
    },[])

    return(
        <>
            <ProductList/>
            <ProductDetails/>
        </>
    )
}

export async function getStaticProps(context) {
    return {
      props: {  products: await fetchProducts() }
    }
  }