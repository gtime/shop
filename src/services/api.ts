import fetch from 'isomorphic-fetch';
import { normalizeProducts } from './utils';


const URL = 'http://www.mocky.io/v2/5c3e15e63500006e003e9795';

export async function fetchProducts() { 
    try {
        const res = await fetch(URL, { method: 'GET' })
        
        if(!res.ok) throw new Error(res.statusText)

        return normalizeProducts((await res.json()).products)
    } catch (e) {
        console.log(e.message);
        
        // throw new Error('Failed to fetch products')
    }
 }