import { appReducer } from "./app";
import { initialState } from "./initials";
import { Action, DenormalizedProduct, NormalizedProduct } from "./types";
import { normalizeProducts } from "./utils";


test('normalizeProducts', () => {    
    const result = normalizeProducts(denormalizedProducts);    
    expect(result).toEqual(normalizedProducts)
})


// Product Test
test('product:requestedFetch', () => {
    const action: Action = { type: 'product:requestedFetch', }
    const result = appReducer(initialState, action);

    expect(result.fetchProduct.loading).toEqual(true);
})

test('product:fetched', () => {
    const expected = {loading: false, done: true, error: null}
    const action: Action = { type: 'product:fetched', payload: normalizedProducts }
    const result = appReducer(initialState, action);

    expect(result.products).toEqual(normalizedProducts.products)
    expect(result.fetchProduct).toEqual(expected)
})

test('product:rejectedFetch', () => {
    const expected = {loading: false, done: false, error: 'Could not fetch products'}
    const action: Action = { type: 'product:rejectedFetch', payload: expected.error }
    const result = appReducer(initialState, action);

    expect(result.fetchProduct).toEqual(expected)
})

// test('ProductTest:fetched', () => {
//     const action: Action = { type: 'product:fetched', payload: normalizedProducts }
//     const result = appReducer(initialState, action);
//     console.log(result);
    
//     // expect(result.prices).toBe(normalizedProducts.products);
//     expect(result.prices).toEqual(normalizedProducts.products);
// })


const denormalizedProducts: DenormalizedProduct[] = [
    {
        "id": 1,
        "name": "Exforge 10mg",
        "prices": [
            {
                "id": 1,
                "price": 10.99,
                "date": "2019-01-01T17:16:32+00:00"
            },
            {
                "id": 2,
                "price": 9.20,
                "date": "2018-11-01T17:16:32+00:00"
            }
        ]
    },
    {
        "id": 2,
        "name": "Exforge 20mg",
        "prices": [
            {
                "id": 3,
                "price": 12.00,
                "date": "2019-01-01T17:16:32+00:00"
            },
            {
                "id": 4,
                "price": 13.20,
                "date": "2018-11-01T17:16:32+00:00"
            }
        ]
    },
    {
        "id": 3,
        "name": "Paracetamol 20MG",
        "prices": [
            {
                "id": 5,
                "price": 5.00,
                "date": "2017-01-01T17:16:32+00:00"
            },
            {
                "id": 6,
                "price": 13.20,
                "date": "2018-11-01T17:16:32+00:00"
            }
        ]
    }
]    

const normalizedProducts: NormalizedProduct = {                                                                       
    prices: { 
      '1': { id: 1, price: 10.99, date: '2019-01-01T17:16:32+00:00' },    
      '2': { id: 2, price: 9.2, date: '2018-11-01T17:16:32+00:00' },      
      '3': { id: 3, price: 12, date: '2019-01-01T17:16:32+00:00' },       
      '4': { id: 4, price: 13.2, date: '2018-11-01T17:16:32+00:00' },     
      '5': { id: 5, price: 5, date: '2017-01-01T17:16:32+00:00' },
      '6': { id: 6, price: 13.2, date: '2018-11-01T17:16:32+00:00' }      
    },
    products: {
        '1': { id: 1, name: 'Exforge 10mg', prices: [ 1, 2 ] },
        '2': { id: 2, name: 'Exforge 20mg', prices: [ 3, 4 ] },
        '3': { id: 3, name: 'Paracetamol 20MG', prices: [ 5, 6 ] }
    }
}