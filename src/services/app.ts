import { createStore,} from 'redux';
import { AppState, Action,  } from './types';
import { initialState } from './initials'; 
import { applyMiddlewares } from './middleware';

// Reducers
export function appReducer(state: AppState = initialState, action: Action):AppState {
    // Fetch Events
    if(action.type === 'product:requestedFetch') return {  
        ...state,  
        fetchProduct: { ...state.fetchProduct, loading: true, done: false }
    }
    
    else if(action.type === 'product:fetched') return {  
        ...state,  
        products: action.payload.products,
        prices: action.payload.prices,
        fetchProduct: { ...state.fetchProduct, loading: false, done: true, error: null }
    }

    else if(action.type === 'product:rejectedFetch') return {  
        ...state,
        fetchProduct: { ...state.fetchProduct, loading: false, error: action.payload }
    }

    // Add Event
    else if(action.type === 'product:requestedAdd') return {
        ...state,  
        addProduct: { ...state.addProduct, loading: true, done: false }
    }

    else if(action.type === 'product:added') {
        const newProduct = {
            ...action.payload,
            id: Object.values(state.products).length,
        }

        return {  
            ...state,  
            products: { ...state.products, [newProduct.id]: newProduct },
            addProduct: { ...state.addProduct, loading: false, error: null }
        }
    }

    else if(action.type === 'product:rejectedAdd') return {  
        ...state,  
        addProduct: { 
            ...state.addProduct, 
            loading: false, 
            error: action.payload 
        }
    }

     // Edit Event
     else if(action.type === 'product:requestedEdit') return {
        ...state,  
        editProduct: { ...state.editProduct, loading: true, done: false }
    }

    else if(action.type ===  'product:edited') {
        return {  
            ...state,  
            products: { ...state.products, [action.payload.id]: action.payload },
            editProduct: { ...state.editProduct, loading: false, error: null }
        }
    }

    else if(action.type === 'product:rejectedEdit') return {  
        ...state,  
        editProduct: { 
            ...state.editProduct, 
            loading: false, 
            error: action.payload 
        }
    }

    // Delete Event
    else if(action.type === 'product:requestedDelete') return {
        ...state,  
        deleteProduct: { ...state.deleteProduct, loading: true, done: false }
    }

    else if(action.type ===  'product:deleted') {
        const products = { ...state.products };
        const product = products[action.payload];
        const prices = { ...state.prices }

        // Delete product prices
        product.prices.forEach(id => delete prices[id])

        // Delete Product
        delete products[product.id]

        return {  
            ...state,  
            products: { ...products },
            prices: { ...prices },
            deleteProduct: { ...state.deleteProduct, loading: false, error: null, done: true }
        }
    }

    else if(action.type === 'product:rejectedDelete') return {  
        ...state,  
        deleteProduct: { 
            ...state.deleteProduct, 
            loading: false, 
            error: action.payload 
        }
    }


    /*  Price */
    // Add Event
    else if(action.type === 'price:requestedAdd') return {
        ...state,  
        addPrice: { ...state.addPrice, loading: true, done: false }
    }

    else if(action.type === 'price:added') {
        const newPrice = {
            ...action.payload.price,
            id: Object.values(state.prices).length,
        }

        const product = state.products[action.payload.productId]
        product.prices = [ ...product.prices, newPrice.id ]

        return {  
            ...state,  
            products: { ...state.products, [product.id]: product },
            prices: { ...state.prices, [newPrice.id]: newPrice },
            addPrice: { 
                ...state.addPrice, 
                loading: false, 
                error: null,
                done: true
            }
        }
    }

    else if(action.type === 'price:rejectedAdd') return {  
        ...state,  
        addPrice: { 
            ...state.addPrice, 
            loading: false, 
            done: false,
            error: action.payload 
        }
    }

     // Edit Event
     else if(action.type === 'price:requestedEdit') return {
        ...state,  
        editPrice: { ...state.editPrice, loading: true, done: false, error: null }
    }

    else if(action.type ===  'price:edited') {
        return {  
            ...state,  
            prices: { ...state.prices, [action.payload.id]: action.payload },
            editPrice: { ...state.editPrice, loading: false, error: null }
        }
    }

    else if(action.type === 'price:rejectedEdit') return {  
        ...state,  
        editPrice: { 
            ...state.editPrice, 
            loading: false, 
            done: false,
            error: action.payload 
        }
    }

    // Delete Event
    else if(action.type === 'price:requestedDelete') return {
        ...state,  
        deletePrice: { ...state.deletePrice, loading: true, done: false }
    }

    else if(action.type ===  'price:deleted') {
        const prices = { ...state.prices };
        const product = state.products[action.payload.productId]
        const price = prices[action.payload.id];

        // Delete product price
        product.prices = product.prices.filter((i) => i != price.id)

        // Delete Price
        delete prices[price.id]

        return {  
            ...state,  
            prices: { ...prices },
            products: { ...state.products, [product.id]: product },
            deletePrice: { 
                ...state.deletePrice,
                loading: false, 
                error: null, 
                done: true 
            }
        }
    }

    else if(action.type === 'price:rejectedDelete') return {  
        ...state,  
        deletePrice: { 
            ...state.deletePrice, 
            loading: false, 
            done: false, 
            error: action.payload
        }
    }

    else return state
}

export const appStore = createStore(
    appReducer,  
    applyMiddlewares()
);