import {  AppState, } from './types'

const change = {
    loading: false,
    error: null,
    done: false,
}

export const initialState: AppState = {
    products: {},
    prices: {},

    // Product CRUD state
    fetchProduct: {...change},
    addProduct: {...change},
    editProduct: {...change},
    deleteProduct: {...change},


    // Price CRUD state
    fetchPrice: {...change},
    addPrice: {...change},
    editPrice: {...change},
    deletePrice: {...change},
}

