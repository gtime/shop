import { applyMiddleware as reduxApplyMiddlewares, MiddlewareAPI } from 'redux';
import { Action, AppDispatch, AppState } from './types';
import { fetchProducts } from './api';


export function applyMiddlewares() {
    return reduxApplyMiddlewares(product, price, storage)
}

// Storage middleware
const storage = (storeAPI: MiddlewareAPI) => (next:AppDispatch) => (action: Action) => {
    const result = next(action);

    if (localStorage)  {
        if(action.type === 'app:loaded') {
            const presistedState = localStorage.getItem('shp:v1');

            if(presistedState) return next({ 
                type: 'app:updatedState', 
                payload: JSON.parse(presistedState) as AppState 
            })
        }
    
        // Storage to local storage
        localStorage.setItem('shop:v1', JSON.stringify(storeAPI.getState()))
    }

    return result;
}



// Product middleware
const URL = 'http://www.mocky.io/v2/5c3e15e63500006e003e9795';
const product = (storeAPI: MiddlewareAPI) => (next: AppDispatch) => (action: Action) => {
    // Prevent non product actions
    if(!action.type.startsWith('product:')) return next(action);

    // Fetch Request
    else if(action.type === 'product:requestedFetch') {
        fetchProducts()
            .then(data => storeAPI.dispatch({type: 'product:fetched', payload: data }))
            .catch(e => storeAPI.dispatch({type: 'product:rejectedFetch', payload: e.message }))
    }
    
    // Add Request: validate new product
    else if(action.type === 'product:requestedAdd') {
        if(!action.payload.name) storeAPI.dispatch({
            type: 'product:rejectedAdd', 
            payload: 'Product name is required' 
        })

        else storeAPI.dispatch({
            type: 'product:added', 
            payload: action.payload 
        }) 
    }

    // Edit Request: validate product
    else if(action.type === 'product:requestedEdit') {
        if(!action.payload.name) storeAPI.dispatch({
            type: 'product:rejectedEdit', 
            payload: 'Product name is required' 
        })

        else storeAPI.dispatch({
            type: 'product:edited', 
            payload: action.payload
        })
    }

    // Edit Request: validate product id
    else if(action.type === 'product:requestedDelete') {
        if(!action.payload) storeAPI.dispatch({
            type: 'product:rejectedDelete', 
            payload: 'Failed to delete product' 
        })

        else storeAPI.dispatch({
            type: 'product:deleted', 
            payload: action.payload
        })
    }

    else return next(action);
} 

// Price middleware
const price = (storeAPI: MiddlewareAPI) => (next: AppDispatch) => (action: Action) => {
    // Prevent non price actions
    if(!action.type.startsWith('price:')) return next(action);

    // Add Request: validate new price
    else if(action.type === 'price:requestedAdd') {
        let message = []

        if(!action.payload.productId) message.push('Product id is required');
        if(!action.payload.price.price) message.push('Price is required');
        if(!action.payload.price.date) message.push('Date is required');
        if(action.payload.price.price < 0) message.push('Price cannot be negative');

        if(message.length > 0) storeAPI.dispatch({
            type: 'price:rejectedAdd', 
            payload: message.join(',')
        })

        else storeAPI.dispatch({
            type: 'price:added', 
            payload: action.payload
        })
    }

    // Edit Request: validate price
    else if(action.type === 'price:requestedEdit') {
        let message = []

        if(!action.payload.id) message.push('Price id is required');
        if(!action.payload.price) message.push('Price is required');
        if(!action.payload.date) message.push('Date is required');
        if(action.payload.price < 0) message.push('Price cannot be negative');

        // Dispathes
        if(message.length > 0) storeAPI.dispatch({
            type: 'price:rejectedAdd', 
            payload: message.join(',')
        })

        else storeAPI.dispatch({
            type: 'price:added', 
            payload: action.payload
        })
    }

    // Edit Request: validate price id
    else if(action.type === 'price:requestedDelete') {
        let message = []

        if(!action.payload.productId) message.push('Product id is required');
        if(!action.payload.id) message.push('Price id is required');

        // Dispatches
        if(!action.payload.id) storeAPI.dispatch({
            type: 'price:rejectedDelete', 
            payload: message
        })

        else storeAPI.dispatch({
            type: 'price:deleted', 
            payload: action.payload
        })
    }


    else return next(action);
} 