import { Dispatch } from 'redux';

// Type Definition
export interface Product {
    id: number;
    name: string;
    prices: number[];
}

export interface NewProduct {
    name: string;
    prices: Price[];
}

export interface Price {
    id: number;
    price: number;
    date: string;
}

export interface NewPrice {
    price: number;
    date: string;
}

export interface DenormalizedProduct {
    id: number;
    name: string;
    prices: Price[];
}

export interface NormalizedProduct {
    products: Record<string, Product>;
    prices: Record<string, Price>;
}

interface Change {
    loading: boolean;
    error: string | null;
    done: boolean;
}

export interface AppState {
    products: Record<string, Product>;
    prices: Record<string, Price>;

    // CRUD
    fetchProduct: Change;
    addProduct: Change;
    editProduct: Change;
    deleteProduct: Change;

    // CRUD
    fetchPrice: Change;
    addPrice: Change;
    editPrice: Change;
    deletePrice: Change;
}

export type Action = 
    { type: 'app:loaded' }|
    { type: 'app:updatedState', payload: AppState }|

    { type: 'product:requestedFetch' }|
    { type: 'product:rejectedFetch', payload: string }|
    { type: 'product:fetched', payload: any }|

    { type: 'product:requestedAdd', payload: NewProduct }|
    { type: 'product:rejectedAdd', payload: string }|
    { type: 'product:added', payload: NewProduct }|

    { type: 'product:requestedEdit', payload: Product }|
    { type: 'product:rejectedEdit', payload: string }|
    { type: 'product:edited', payload: Product } |
    
    { type: 'product:requestedDelete', payload: string }|
    { type: 'product:rejectedDelete', payload: string }|
    { type: 'product:deleted', payload: string }|

    /* Price */ 
    { type: 'price:requestedAdd', payload: { productId: string, price: NewPrice } }|
    { type: 'price:rejectedAdd', payload: string }|
    { type: 'price:added', payload: { productId: string, price: NewPrice } }|

    { type: 'price:requestedEdit', payload: Price }|
    { type: 'price:rejectedEdit', payload: string }|
    { type: 'price:edited', payload: Price } |
    
    { type: 'price:requestedDelete', payload: { productId: string, id: string } }|
    { type: 'price:rejectedDelete', payload: string }|
    { type: 'price:deleted', payload: { productId: string, id: string } };

export type AppDispatch = Dispatch<Action>