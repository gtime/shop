import { normalize, schema } from 'normalizr';
import {DenormalizedProduct, AppState, NormalizedProduct, Product, Price} from './types'

export function normalizeProducts(products: DenormalizedProduct[]) {
    const price = new schema.Entity('prices');
    const productSchema = new schema.Entity('products', { prices: [price] });
    const productsSchema = new schema.Array(productSchema);
    const result = normalize(products, productsSchema);

    return result.entities
}

export function normalizeProduct(product: DenormalizedProduct) {
    const price = new schema.Entity('prices');
    const productSchema = new schema.Entity('products', { prices: [price] });
    const result = normalize(product, productSchema);

    return result.entities
}
